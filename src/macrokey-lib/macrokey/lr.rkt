#!/usr/bin/env racket


;; This file is part of racket-macrokey.

;; racket-macrokey is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-macrokey is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-macrokey.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later


#lang racket/base

(require
 (for-syntax racket/base syntax/parse)
 "keyset.rkt"
 )

(provide
 (except-out (all-from-out racket/base) #%module-begin)
 (rename-out [module-begin #%module-begin])
 )


(define-syntax (module-begin stx)
  (syntax-parse stx
    [(_
      body ...
      #:KeySet (v ...) ...
      )
     #'(#%module-begin
        (provide provided-keysets)
        body ...
        (define provided-keysets
          (list (keyset v ... ) ... ))
        )]
    ))
