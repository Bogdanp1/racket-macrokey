#!/usr/bin/env racket


;; This file is part of racket-macrokey.

;; racket-macrokey is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-macrokey is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-macrokey.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later


#lang racket/base

(provide (all-defined-out))


(struct keyaction
  (key position action)
  #:transparent
  #:mutable
  #:guard
  (lambda (key position action name)
    (unless (string? key)
      (error 'keyaction-guard "not a valid key (string): ~a" key))
    (unless (member position '(absolute left right))
      (error 'keyaction-guard "not a valid position: ~a" position))
    (unless (member action '(press release))
      (error 'keyaction-guard "not a valid action: ~a" position))
    (values key position action)
    )
  )


(define (string->keyaction str)
  (keyaction
   (regexp-replace #rx"_[LR]$" (regexp-replace #rx"^[\\+\\-]" str "") "")
   (cond
     [(regexp-match-exact? #rx".*_L$" str)  'left]
     [(regexp-match-exact? #rx".*_R$" str)  'right]
     [else  'absolute]
     )
   (if (regexp-match-exact? #rx"^\\+.*" str) 'press 'release)
   ))


(module+ test

  (require rackunit)

  (check-equal?
   (string->keyaction "+a")  (keyaction "a" 'absolute 'press))
  (check-equal?
   (string->keyaction "+Control_L")  (keyaction "Control" 'left 'press))
  (check-equal?
   (string->keyaction "-Control_R")  (keyaction "Control" 'right 'release))

  )
