#!/usr/bin/env racket


;; This file is part of racket-macrokey.

;; racket-macrokey is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, package-version 3.

;; racket-macrokey is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-macrokey.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later


#lang scribble/manual

@(require macrokey/version)


@title[#:tag "macrokey"]{Racket-MacroKey}

@author[@author+email["Maciej Barć" "xgqt@riseup.net"]]


MacroKey is a tool written in Racket that allows you to run
specific commands on key presses declared with a simple DSL.

Version: @VERSION


@table-of-contents[]


@index-section[]
