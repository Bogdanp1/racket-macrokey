# MacroKey


## About

MacroKey is a tool written in Racket that allows you to run
specific commands on key presses declared with a simple DSL.

For logging keys on X11 it uses
[XKBCAT](https://github.com/anko/xkbcat/).
